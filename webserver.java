import java.io.* ;    
import java.net.* ;
import java.util.* ;


public final class webserver
{
    public static void main(String argv[]) throws Exception
    {
      ServerSocket pSocket = new ServerSocket(2407);//declaramos conexion para el 2407

      while (true) {
        System.out.println("Esperando conexion...");
        Socket conexion = pSocket.accept(); //instanseamos y usamos este metodo para aceptar el socket
        HttpRequest request = new HttpRequest(conexion); //instanseamos y mandamos la conexion al request
        Thread thread = new Thread(request); //Hacemos una instancia de thread para  el request
        thread.start(); //Inciamos con el thread 
      }
    }
}


final class HttpRequest implements Runnable
{
      final static String CRLF = "\r\n"; // no se exactamente como funciona pero seignifica Carriage return(CR) and line feed (FD)
      Socket socket; 
      
      public HttpRequest(Socket socket) throws Exception{
          this.socket = socket;
      }//Constructor
      
      public void run(){try {processRequest();}catch (Exception e) {System.out.println(e);}} //implementamos el run de runnable para que  maneje aca las esepciones del bloque

      private void processRequest() throws Exception{
        //Referencia para los flujos de entrada y salida
          InputStream inFiltro = socket.getInputStream();
          DataOutputStream a = new DataOutputStream(socket.getOutputStream());
          BufferedReader br = new BufferedReader(new InputStreamReader(inFiltro));//Configurar filtros
          
          //Aca obtenemos la linea de peticion http
              String lineahttp = br.readLine();
          //Despliega la linea
              System.out.println();
              System.out.println(lineahttp);


          StringTokenizer parteslinea = new StringTokenizer(lineahttp);//tratamos la linea y extraemos con delimitadores
          parteslinea.nextToken(); 
          String fileName = parteslinea.nextToken();
          fileName = "." + fileName;//se pone . porque esta en el mismo directorio

          FileInputStream archivo = null; //Abrimos el archivo requerido
          boolean existe = true;
      try {
          archivo = new FileInputStream(fileName);}
          catch (FileNotFoundException e) { existe = false;}

      String status = null;
      String contenidol = null;
      String cuerpo = null;

        if (existe) {
           status = "HTTP/1.0 200 OK" + CRLF; 
           contenidol = "Content : " + contentType( fileName ) + CRLF;
        }else{
           status = "HTTP/1.0 404 Not Found" + CRLF;
           contenidol = "Content-type: " + "text/html" + CRLF;
           cuerpo = "<HTML>" +
            "<HEAD><TITLE>Not Found</TITLE></HEAD>" +
            "<BODY>Not Found</BODY></HTML>";
          }
          a.writeBytes(status);//Estado de linea GET
          a.writeBytes(contenidol); //estado de linea de contenido HEAD
          a.writeBytes(CRLF); //envia lineas en blanco para el HEAD

        if (existe) {
             sendBytes(archivo, a);
             a.writeBytes(status);
             a.writeBytes(contenidol);
             archivo.close();
        }else{
             a.writeBytes(status);
             a.writeBytes(cuerpo);//Error
             a.writeBytes(contenidol);
        }
            System.out.println(fileName); //imprime el nombre del archivo
        
        //obtiene y muestra lineas de cabecera
        String headerLine = null;
        while ((headerLine = br.readLine()).length() != 0) {
            System.out.println(headerLine);}

        //Cerramos los streams y sockets
        a.close();
        br.close();
        socket.close();



}
          private static String contentType(String fileName)
          {
           if(fileName.endsWith(".htm") || fileName.endsWith(".html")) {return "text/html";}
           if(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg")) {return "image/jpeg";}
           if(fileName.endsWith(".gif")) {return "image/gif";}
           return "application/octet-stream";
          }

          //Flujos salida y entrada
          private static void sendBytes(FileInputStream archivo, OutputStream a) throws Exception
          {
              byte[] buffer = new byte[1024]; //Construye bufeer para mantener bytes en camino
              int bytes = 0; 

            while((bytes = archivo.read(buffer)) != -1 )
              {
              a.write(buffer, 0, bytes);
              }
          }
}