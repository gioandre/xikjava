public class Persona extends Person{
	String name, lastname, employment;
	int age;

		public Persona(String b, String c, int d, String f){
		super(b,c,d,f);
		this.name = b;
		this.lastname = c;
		this.employment = f;
		this.age = d;
	}

	public String getname(){
		return name;
	}

	public String getlastname(){
		return lastname;
	}

	public int getage(){
		return age;
	}

	public String getemploymentsituation(){
		return employment;
	}
}