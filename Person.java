public abstract class Person{
	protected String name, lastname,employmentsituation;
	protected int age;

	public Person(String b, String c, int d, String f){
		this.name = b;
		this.lastname = c;
		this.age= d;
		this.employmentsituation = f;
	}

	public abstract String getname();
	public abstract String getlastname();
	public abstract int getage();
	public abstract String getemploymentsituation();
	
}