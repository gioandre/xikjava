package visualTests;

import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio3 extends Beforeclass{
		@Test
		public void f() {
			try {

			ReadUrlFile.Wait(10000);
			screen.wait("img/ejercicio2/add.png").click();
			
			
			ReadUrlFile.Wait(7000);
			Region region2 = screen.wait("img/ejercicio2/foldername2.png");
			region2.click();
			region2.paste("MoveFolder");
			
			ReadUrlFile.Wait(1000);
			screen.wait("img/ejercicio2/Nd.png").click();
			
			ReadUrlFile.Wait(5000);
			Region a = screen.wait("img/ejercicio3/Mv.png");
			
			ReadUrlFile.Wait(5000);
			a.wait("img/ejercicio3/bll.png").click();
			
			ReadUrlFile.Wait(3000);
			screen.wait("img/ejercicio3/m.png").click();
			
			ReadUrlFile.Wait(5000);
			screen.wait("img/ejercicio3/trash.png").click();
			
			ReadUrlFile.Wait(3000);
			screen.wait("img/ejercicio3/save.png").click();
			
			ReadUrlFile.Wait(3000);
			Region b = screen.wait("img/ejercicio3/tree.png");
			
			ReadUrlFile.Wait(5000);
			b.wait("img/ejercicio3/treetrash.png").click();
			b.click();
			
			
			ReadUrlFile.Wait(5000);
			screen.wait("img/ejercicio3/Mv.png").click();
			
			
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
