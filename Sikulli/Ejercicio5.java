package visualTests;

import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio5 extends Beforeclass{
		@Test
		public void f() {
			try {
	
			
			ReadUrlFile.Wait(10000);
			screen.wait("img/ejercicio2/add.png").click();
			
			
			ReadUrlFile.Wait(7000);
			Region region2 = screen.wait("img/ejercicio2/foldername2.png");
			region2.click();
			region2.paste("D1");
			ReadUrlFile.Wait(1000);
			screen.wait("img/ejercicio2/Nd.png").click();
			
			ReadUrlFile.Wait(1000);
			screen.wait("img/ejercicio2/add.png").click();
			ReadUrlFile.Wait(1000);
			Region region3 = screen.wait("img/ejercicio2/foldername2.png");
			region3.click();
			region3.paste("D2");
			ReadUrlFile.Wait(1000);
			screen.wait("img/ejercicio2/Nd.png").click();
			
			ReadUrlFile.Wait(2000);
			Region region4 = screen.wait("img/ejercicio5/D1.png");
			region4.wait("img/ejercicio5/cd1.png");
			
			ReadUrlFile.Wait(2000);
			Region region5 = screen.wait("img/ejercicio5/tree.png");
			region5.wait("img/ejercicio5/td2.png");
			
			ReadUrlFile.Wait(4000);
			screen.dragDrop(region5, region4);		
			
			ReadUrlFile.Wait(4000);
			screen.wait("img/ejercicio5/D1carpeta.png").click();
			ReadUrlFile.Wait(4000);
			screen.wait("img/ejercicio5/D2carpeta.png").click();
			
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
