package visualTests;

import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio4 extends Beforeclass{
		@Test
		public void f() {
			try {
	
			ReadUrlFile.Wait(10000);
			screen.wait("img/ejercicio4/add.png").click();
			
			ReadUrlFile.Wait(10000);
			screen.wait("img/ejercicio4/addf.png").click();
			
			
			
			
			ReadUrlFile.Wait(7000);
			Region region2 = screen.wait("img/ejercicio4/panel.png");
			region2.wait("img/ejercicio4/test.png").click();
			region2.click();
			
			
			
			ReadUrlFile.Wait(1000);
			screen.wait("img/ejercicio4/open.png").click();
			
			ReadUrlFile.Wait(7000);
			Region region3 = screen.wait("img/ejercicio4/upload.png");
			region3.wait("img/ejercicio4/up.png").click();
			region3.click();
			
			
			
			ReadUrlFile.Wait(10000);
			screen.wait("img/ejercicio4/mf.png").click();
			
			ReadUrlFile.Wait(5000);
			screen.wait("img/ejercicio4/gostart.png").click();
			
			ReadUrlFile.Wait(9000);
			screen.wait("img/ejercicio4/verify.png").click();
			
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
