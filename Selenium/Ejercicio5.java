package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

import java.awt.Robot;
import java.awt.event.InputEvent;


public class Ejercicio5 extends Browsers{

	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("email")).sendKeys("testx4@gmail.com");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("password")).sendKeys("Ab1234");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("bt_send")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder_name")).sendKeys("D1");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//button[contains(.,'New directory')]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder_name")).sendKeys("D2");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//button[contains(.,'New directory')]")).click();
			ReadUrlFile.Wait(2000);
									
			Robot robot = new Robot();
			
			ReadUrlFile.Wait(1000);
			robot.mouseMove(550, 400);
			ReadUrlFile.Wait(1000);
			robot.mousePress(InputEvent.BUTTON1_MASK);
			robot.delay(1000);
			robot.mouseMove(680, 360);
			robot.delay(1000);
			robot.mouseRelease(InputEvent.BUTTON1_MASK);
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='D2']/../div[2]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='D1']/../div[2]")).click();
			
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}