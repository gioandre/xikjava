package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;



/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio4 extends Browsers{

	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("email")).sendKeys("testx4@gmail.com");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("password")).sendKeys("Ab1234");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("bt_send")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("add")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//label[contains(.,'Add files')]")).click();
									
			Robot robot = new Robot();
			
			robot.delay(200);
			robot.keyPress(KeyEvent.VK_T);
			robot.keyPress(KeyEvent.VK_E);
			robot.keyPress(KeyEvent.VK_S);
			robot.keyPress(KeyEvent.VK_T);
			robot.keyPress(KeyEvent.VK_PERIOD);
			robot.keyPress(KeyEvent.VK_T);
			robot.keyPress(KeyEvent.VK_X);
			robot.keyPress(KeyEvent.VK_T);
			robot.delay(200);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.delay(200);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.delay(200);
			robot.keyPress(KeyEvent.VK_ENTER);
			
			ReadUrlFile.Wait(5000);
			robot.mouseMove(1080, 280);
			
			robot.mousePress(InputEvent.BUTTON1_MASK);
			robot.delay(200);
			robot.mouseRelease(InputEvent.BUTTON1_MASK);
			
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("buttonMenu")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath(".//*[@id='dropdownMenu']/ul/li/a[2]")).click();
			ReadUrlFile.Wait(4000);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='test.TXT']/../div[2]")).click();
			
			
			
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}