package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio1 extends Browsers{

	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("email")).sendKeys("testx4@gmail.com");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("password")).sendKeys("Ab1234");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("bt_send")).click();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
