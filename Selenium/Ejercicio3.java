package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio3 extends Browsers{

	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("email")).sendKeys("testx4@gmail.com");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("password")).sendKeys("Ab1234");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("bt_send")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder_name")).sendKeys("moveFolder");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//button[contains(.,'New directory')]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='moveFolder']/../div[3]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("context_move")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//div[7]/div[2]/ul/li/ul/li[last()]/a[last()]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//div[7]/div[3]/div/button/span[@class='ui-button-text']")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='trashbin']/../div[2]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='moveFolder']/../div[2]")).click();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}