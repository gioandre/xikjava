package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
/**
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class SeleniumTest extends Browsers{

	@Test
	public void f() {
		try {
			driver.get("http://www.google.com.gt");
		driver.findElement(By.id("lst-ib")).sendKeys("xik.gt\n");
			
			ReadUrlFile.Wait(2000);
			driver.findElement(By.linkText("xik.gt")).click();
			ReadUrlFile.Wait(3000);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
