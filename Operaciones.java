public class Operaciones extends Calculadora{
	double rsum, rres, rmult, rdiv, a, b;

	public Operaciones(double g, double d){
		super(g,d);
		this.a = g;
		this.b = d;

	}

	public double suma(){
		rsum = a + b;
		return rsum;
	}

	public double resta(){
		rres = a - b;
		return rres;
	}

	public double multiply(){
		rmult = a * b;
		return rmult;
	}

	public double divi(){
		rdiv = a / b;
		return rdiv;
	}
	
}