import java.util.Scanner;
import java.io.*;

public class Ejercicio4 {
		public static void main(String[] args) throws Exception{
		BufferedReader i = new BufferedReader (new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);

		System.out.println("Pleas give us the radios \n");
		double r = Double.parseDouble(sc.nextLine());		

		Circle a = new Circle(r);
		Sphere b = new Sphere(r);


		System.out.println("Circle Characteristics");
		System.out.print("Side: ");
		System.out.println(r);
		System.out.print("Diameter: ");
		System.out.println(a.calculateDiameter());
		System.out.print("Circumference: ");
		System.out.println(a.calculateCircumference());
		System.out.print("Area: ");
		System.out.println(a.calculateArea());
		System.out.print("\n \n \n");
		


		System.out.println("Sphere Characteristics");
		System.out.print("Side: ");
		System.out.println(r);
		System.out.print("Diameter: ");
		System.out.println(b.calculateDiameter());
		System.out.print("Circumference: ");
		System.out.println(b.calculateCircumference());
		System.out.print("Area: ");
		System.out.println(b.calculateArea());
		
	}
}