
function Humano(nom, age, co) {
    this.nombre = nom;
    this.edad = age;
    this.color = co;
}
Humano.prototype.name = function() {
    console.log(this.nombre);
    return this.nombre;
}; 
Humano.prototype.old = function() {
    console.log(this.edad);
    return this.edad;
}; 

Humano.prototype.toJSON = function() {
    console.log("name: "+ this.nombre + " old: " + this.edad + " color: " + this.color);
    return "name: "+ this.nombre + " old: " + this.edad + " color: " + this.color;
}; 

var miguel = new Humano("miguel", 20, "azul");
miguel.name();
miguel.old();
miguel.toJSON();
